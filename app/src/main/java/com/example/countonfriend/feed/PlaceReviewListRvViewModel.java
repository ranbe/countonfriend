package com.example.countonfriend.feed;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;

import java.util.List;

public class PlaceReviewListRvViewModel extends ViewModel {
    LiveData<List<PlaceReview>> data;

    public PlaceReviewListRvViewModel(){
        data = Model.instance.getAll();
    }
    public LiveData<List<PlaceReview>> getData() {
        List<PlaceReview> data_value = data.getValue();
        if (data_value != null) {
            for (int i = 0; i < data_value.size(); i++) {
               // Log.d("DEBUG ", data_value.get(i).getName() + " is " + data_value.get(i).getIsDeleted());
                if (data_value.get(i).getIsDeleted()) {
                    data_value.remove(i);
                }
            }
        }
        return data;
    }
}
