package com.example.countonfriend.feed;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.countonfriend.R;
import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;
import com.squareup.picasso.Picasso;

public class PlaceReviewListRvFragment extends Fragment {
    PlaceReviewListRvViewModel viewModel;
    MyAdapter adapter;
    SwipeRefreshLayout swipeRefresh;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(this).get(PlaceReviewListRvViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_review_list,container,false);

        swipeRefresh = view.findViewById(R.id.placeReviewlist_swiperefresh);
        swipeRefresh.setOnRefreshListener(() -> Model.instance.refreshPlaceReviewsList());

        RecyclerView list = view.findViewById(R.id.placeReviewlist_rv);
        list.setHasFixedSize(true);


        list.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new MyAdapter();
        list.setAdapter(adapter);

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v,int position) {
                String stId = viewModel.getData().getValue().get(position).getId();
                Navigation.findNavController(v).navigate(PlaceReviewListRvFragmentDirections.actionPlaceReviewListRvFragmentToPlaceReviewDetailsFragment(stId));

            }
        });

        setHasOptionsMenu(true);
        viewModel.getData().observe(getViewLifecycleOwner(), list1 -> refresh());
        swipeRefresh.setRefreshing(Model.instance.getPlaceReviewListLoadingState().getValue() == Model.PlaceReviewListLoadingState.loading);
        Model.instance.getPlaceReviewListLoadingState().observe(getViewLifecycleOwner(), placeReviewListLoadingState -> {
            if (placeReviewListLoadingState == Model.PlaceReviewListLoadingState.loading){
                swipeRefresh.setRefreshing(true);
            }else{
                swipeRefresh.setRefreshing(false);
            }

        });
        return view;
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView placeImageImv;
        TextView nameTv;
        TextView descTv;
        TextView usernameTv;

        public MyViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.listrow_name_tv);
            descTv = itemView.findViewById(R.id.listrow_description_tv);
            placeImageImv = itemView.findViewById(R.id.listrow_place_image_imv);
            usernameTv = itemView.findViewById(R.id.listrow_user_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    listener.onItemClick(v,pos);
                }
            });
        }

        void bind(PlaceReview placeReview){
            nameTv.setText(placeReview.getName());
            descTv.setText(placeReview.getDescription());
            placeImageImv.setImageResource(R.drawable.niagra);
            Model.instance.getUserDetailsById(placeReview.getUid(), (userDetails) -> {
                if (userDetails != null) {
                    usernameTv.setText("Posted by " + userDetails.getUsername());
                }
            });

            if (placeReview.getImageUrl() != null) {
                Picasso.get()
                        .load(placeReview.getImageUrl())
                        .into(placeImageImv);
            }
        }
    }

    interface OnItemClickListener{
        void onItemClick(View v,int position);
    }
    class MyAdapter extends RecyclerView.Adapter<MyViewHolder>{

        OnItemClickListener listener;
        public void setOnItemClickListener(OnItemClickListener listener){
            this.listener = listener;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.place_review_list_row,parent,false);
            MyViewHolder holder = new MyViewHolder(view,listener);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            PlaceReview placeReview = viewModel.getData().getValue().get(position);
            /*if (placeReview.getIsDeleted()) {
                Log.d("DEBUG", placeReview.getName() + " is true");
                holder.itemView.setVisibility(View.GONE);
            }*/
            holder.bind(placeReview);
        }

        @Override
        public int getItemCount() {
            if(viewModel.getData().getValue() == null){
                return 0;
            }
            return viewModel.getData().getValue().size();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.place_review_list_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.addPlaceReviewFragment){
            Log.d("TAG","ADD...");
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }
}