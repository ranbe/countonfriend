package com.example.countonfriend.feed;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.countonfriend.R;
import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;

public class AddPlaceReviewFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    EditText nameEt;
    EditText descEt;
    Button saveBtn;
    Button cancelBtn;
    ProgressBar progressBar;
    Bitmap imageBitmap;
    ImageView placeImgImv;
    ImageButton camBtn;
    ImageButton galleryBtn;
    String userName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_place_review,container, false);
        nameEt = view.findViewById(R.id.main_name_et);
        descEt = view.findViewById(R.id.main_description_et);
        saveBtn = view.findViewById(R.id.main_save_btn);
        cancelBtn = view.findViewById(R.id.main_cancel_btn);
        progressBar = view.findViewById(R.id.main_progressbar);
        progressBar.setVisibility(View.GONE);
        placeImgImv = view.findViewById(R.id.main_place_image_imv);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(nameEt).navigateUp();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEt.getText().toString();
                if (name.length() < 3) {
                    nameEt.setError("Place name should have at least 3 characters");
                    return;
                }
                if (!name.matches("[a-zA-Z\\s]+")) {
                    nameEt.setError("Place name shouldn't contain symbols/numbers");
                    return;
                }

                String description = descEt.getText().toString();
                if (description.length() < 10) {
                    descEt.setError("Description should have at least 10 characters");
                    return;
                }

                Model.instance.getPlaceReviewByNameAndUid(FirebaseAuth.getInstance().getUid(), name, (placeReview) -> {
                        if ((placeReview != null && placeReview.getIsDeleted()) || (placeReview == null)) {
                            save();
                        } else {
                            nameEt.setError("You already posted about this place, you can edit your post.");
                            nameEt.requestFocus();
                            return;
                        }
                });


            }
        });

        camBtn = view.findViewById(R.id.main_cam_btn);
        galleryBtn = view.findViewById(R.id.main_gallery_btn);

        camBtn.setOnClickListener(v -> {
            openCam();
        });

        galleryBtn.setOnClickListener(v -> {
            openGallery();     
        });
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Model.instance.getUserDetailsById(userId, (userDetails) -> {
            userName = userDetails.getUsername();
        });
        return view;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
    }

    private void openCam() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA){
            if (resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                placeImgImv.setImageBitmap(imageBitmap);

            }
        }
        else if (requestCode == PICK_IMAGE_FROM_GALLERY){
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        imageBitmap = bitmap;
                        placeImgImv.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void save() {
        progressBar.setVisibility(View.VISIBLE);
        saveBtn.setEnabled(false);
        cancelBtn.setEnabled(false);
        camBtn.setEnabled(false);
        galleryBtn.setEnabled(false);

        Model.instance.countReviews((count) -> {
            String name = nameEt.getText().toString();
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String id = count + "-" + uid;
            String description = descEt.getText().toString();
            PlaceReview placeReview = new PlaceReview(name, id, description, uid, userName, false);
            if (imageBitmap == null) {
                Model.instance.addPlaceReview(placeReview,(message)->{
                    if (message == "") {
                        Navigation.findNavController(nameEt).navigateUp();
                    } else {
                        Toast.makeText(getActivity(), "You already made a post about this place", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
            else {
                Model.instance.saveImage(imageBitmap, id + ".jpg", url -> {
                    placeReview.setImageUrl(url);
                    Model.instance.addPlaceReview(placeReview,(message)->{
                        if (message == "") {
                            Navigation.findNavController(nameEt).navigateUp();
                        } else {
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                });
            }
        });
    }
}