package com.example.countonfriend.feed;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class UserReviewsListRvViewModel extends ViewModel {
    LiveData<List<PlaceReview>> data;


    public UserReviewsListRvViewModel() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        data = Model.instance.getAllUserReviews(uid);
    }
    public LiveData<List<PlaceReview>> getData() {
        List<PlaceReview> data_value = data.getValue();
        if (data_value != null) {
            for (int i = 0; i < data_value.size(); i++) {
                if (data_value.get(i).getIsDeleted()) {
                    data_value.remove(i);
                }
            }
        }
        return data;
    }
}
