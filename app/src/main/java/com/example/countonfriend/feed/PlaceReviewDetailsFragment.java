package com.example.countonfriend.feed;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.countonfriend.R;
import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;
import com.example.countonfriend.model.UserDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;


public class PlaceReviewDetailsFragment extends Fragment {
    TextView nameTv;
    TextView usernameTv;
    TextView descTv;
    ImageView placeImageImv;
    Button deleteBtn;
    ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_place_review_details, container, false);
        progressBar = view.findViewById(R.id.details_progressbar);
        progressBar.setVisibility(View.VISIBLE);

        String stId = PlaceReviewDetailsFragmentArgs.fromBundle(getArguments()).getPlaceReviewId();
        deleteBtn = view.findViewById(R.id.details_delete_btn);
        deleteBtn.setOnClickListener(view1 -> {
            Model.instance.deletePlaceReviewById(stId, (message)->{
                if (message == "") {
                    Navigation.findNavController(view1).navigateUp();
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            });
        });

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Model.instance.getUserDetailsById(userId, (userDetails) -> {
                if (userDetails!= null && userDetails.getUserRole() == UserDetails.Role.Admin) {
                    deleteBtn.setVisibility(View.VISIBLE);
                }
        });

        Model.instance.getPlaceReviewById(stId, new Model.GetPlaceReviewById() {
            @Override
            public void onComplete(PlaceReview placeReview) {
                if (placeReview == null) return;

                nameTv.setText(placeReview.getName());
                descTv.setText(placeReview.getDescription());
                descTv.setMovementMethod(new ScrollingMovementMethod());
                Model.instance.getUserDetailsById(placeReview.getUid(), (userDetails) -> {
                    if (userDetails != null) {
                        usernameTv.setText("Posted by " + userDetails.getUsername());
                    }
                    progressBar.setVisibility(View.GONE);
                });
                if (placeReview.getImageUrl() != null) {
                    Picasso.get().load(placeReview.getImageUrl()).into(placeImageImv);
                }
            }
        });

        nameTv = view.findViewById(R.id.details_name_tv);
        descTv = view.findViewById(R.id.details_description_tv);
        placeImageImv = view.findViewById(R.id.details_place_image_img);
        usernameTv = view.findViewById(R.id.details_user_tv);

        Button backBtn = view.findViewById(R.id.details_back_btn);
        backBtn.setOnClickListener((v)->{
            Navigation.findNavController(v).navigateUp();
        });
        return view;
    }
}