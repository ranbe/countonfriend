package com.example.countonfriend.feed;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.countonfriend.R;
import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.PlaceReview;
import com.example.countonfriend.model.UserDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;


public class PlaceReviewEditFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    TextView nameEt;
    TextView descriptionEt;
    ImageView placeImageImv;
    ProgressBar progressBar;
    Bitmap imageBitmap;
    String prevImageUrl;
    String userName;
    ImageButton camBtn;
    ImageButton galleryBtn;
    Button saveBtn;
    Button deleteBtn;
    Button cancelBtn;
    String currId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_place_review, container, false);

        progressBar = view.findViewById(R.id.main_progressbar);
        progressBar.setVisibility(View.GONE);
        saveBtn = view.findViewById(R.id.main_save_btn);
        deleteBtn = view.findViewById(R.id.main_delete_btn);
        deleteBtn.setVisibility(View.VISIBLE);
        nameEt = view.findViewById(R.id.main_name_et);
        //nameEt.setEnabled(false);
        descriptionEt = view.findViewById(R.id.main_description_et);
        placeImageImv = view.findViewById(R.id.main_place_image_imv);
        cancelBtn = view.findViewById(R.id.main_cancel_btn);

        String stId = PlaceReviewEditFragmentArgs.fromBundle(getArguments()).getPlaceReviewId();

        Model.instance.getPlaceReviewById(stId, new Model.GetPlaceReviewById() {
            @Override
            public void onComplete(PlaceReview placeReview) {
                currId = placeReview.getId();
                nameEt.setText(placeReview.getName());
                descriptionEt.setText(placeReview.getDescription());
                if (placeReview.getImageUrl() != null) {
                    Picasso.get().load(placeReview.getImageUrl()).into(placeImageImv);
                    prevImageUrl = placeReview.getImageUrl();
                }
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Model.instance.deletePlaceReviewById(stId, (message)->{
                    if (message == "") {
                        Navigation.findNavController(nameEt).navigateUp();
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = descriptionEt.getText().toString();
                if (description.length() < 10) {
                    descriptionEt.setError("Description should have at least 10 characters");
                    return;
                }

                String name = nameEt.getText().toString();
                if (!name.matches("[a-zA-Z\\s]+")) {
                    nameEt.setError("Place name shouldn't contain symbols/numbers");
                    return;
                }

                save();
            }
        });

        camBtn = view.findViewById(R.id.main_cam_btn);
        galleryBtn = view.findViewById(R.id.main_gallery_btn);

        camBtn.setOnClickListener(v -> {
            openCam();
        });

        galleryBtn.setOnClickListener(v -> {
            openGallery();
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getChildFragmentManager().popBackStackImmediate();
              //  getFragmentManager().popBackStackImmediate();
                Navigation.findNavController(nameEt).navigateUp();
            }
        });
        /*Button backBtn = view.findViewById(R.id.main_);
        backBtn.setOnClickListener((v)->{
            Navigation.findNavController(v).navigateUp();
        });*/

        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Model.instance.getUserDetailsById(userId, (userDetails) -> {
            userName = userDetails.getUsername();
        });
        return view;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
    }

    private void openCam() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA){
            if (resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                placeImageImv.setImageBitmap(imageBitmap);
            }
        }
        else if (requestCode == PICK_IMAGE_FROM_GALLERY){
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        placeImageImv.setImageBitmap(bitmap);
                        imageBitmap = bitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void save() {
        progressBar.setVisibility(View.VISIBLE);
        saveBtn.setEnabled(false);
        deleteBtn.setEnabled(false);
        camBtn.setEnabled(false);
        galleryBtn.setEnabled(false);
        cancelBtn.setEnabled(false);

        String name = nameEt.getText().toString();
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String id = currId;
        String description = descriptionEt.getText().toString();
        Log.d("TAG","saved name:" + name + " id:" + id);

        PlaceReview placeReview = new PlaceReview(name, id, description, uid, userName, false);

        if (imageBitmap == null) {
            placeReview.setImageUrl(prevImageUrl);
            Model.instance.editPlaceReview(currId, placeReview,(message)->{
                if (message == "") {
                    Navigation.findNavController(nameEt).navigateUp();
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            Model.instance.saveImage(imageBitmap, id + ".jpg", url -> {
                placeReview.setImageUrl(url);
                Model.instance.editPlaceReview(currId, placeReview,(message)->{
                    if (message == "") {
                        Navigation.findNavController(nameEt).navigateUp();
                    } else {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
            });
        }
    }
}