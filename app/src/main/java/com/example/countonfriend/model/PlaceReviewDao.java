package com.example.countonfriend.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PlaceReviewDao {

    @Query("select * from PlaceReview")
    List<PlaceReview> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(PlaceReview... placeReviews);

    @Delete
    void delete(PlaceReview placeReview);

    @Query("DELETE from placeReview where id=:id")
    void deleteById(String id);

}
