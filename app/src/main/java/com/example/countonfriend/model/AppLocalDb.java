package com.example.countonfriend.model;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.countonfriend.MyApplication;

@Database(entities = {PlaceReview.class}, version = 4)
abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract PlaceReviewDao placeReviewDao();
}

public class AppLocalDb{
    static public AppLocalDbRepository db =
            Room.databaseBuilder(MyApplication.getContext(),
                    AppLocalDbRepository.class,
                    "dbFileName.db")
                    .fallbackToDestructiveMigration()
                    .build();
}

