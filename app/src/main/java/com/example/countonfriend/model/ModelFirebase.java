package com.example.countonfriend.model;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.auth.User;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ModelFirebase {
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public ModelFirebase() {
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build();
        db.setFirestoreSettings(settings);
    }

    public interface GetAllPlaceReviewsListener {
        void onComplete(List<PlaceReview> list);
    }

    public void getAllPlaceReviews(Long lastUpdateDate, GetAllPlaceReviewsListener listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .whereGreaterThanOrEqualTo("updateDate", new Timestamp(lastUpdateDate, 0))
                .get()
                .addOnCompleteListener(task -> {
                    List<PlaceReview> list = new LinkedList<PlaceReview>();
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot doc : task.getResult()) {
                            PlaceReview placeReview = PlaceReview.create(doc.getData());
                            Log.d("DEBUG", "what " + placeReview.getName() + " " + placeReview.getIsDeleted());
                            if (placeReview != null) {
                                list.add(placeReview);
                            }
                        }
                    }
                    listener.onComplete(list);
                });
    }

    public void getPlaceReviewsByUid(String uid, Model.GetPlaceReviewsByUid listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .whereEqualTo("uid", uid)
                .get()
                .addOnCompleteListener(task -> {
                    List<PlaceReview> list = new LinkedList<PlaceReview>();
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot doc : task.getResult()) {
                            PlaceReview placeReview = PlaceReview.create(doc.getData());
                            if (placeReview != null) {
                                list.add(placeReview);
                            }
                        }
                    }
                    listener.onComplete(list);
                });
    }
    public void countReviews(Model.countReviewsListener listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        listener.onComplete(task.getResult().size());
                    } else {
                        listener.onComplete(0);
                    }
                });
    }
    public void addPlaceReview(PlaceReview placeReview, Model.AddPlaceReviewListener listener) {
        Map<String, Object> json = placeReview.toJson();
        db.collection(PlaceReview.COLLECTION_NAME)
                .document(placeReview.getId())
                .set(json)
                .addOnSuccessListener(unused -> listener.onComplete(""))
                .addOnFailureListener(e -> listener.onComplete("Failed to add post"));
    }

    public void editPlaceReview(String currId, PlaceReview placeReview, Model.EditPlaceReviewListener listener) {
        Map<String, Object> json = placeReview.toJson();
        db.collection(PlaceReview.COLLECTION_NAME)
                .document(currId)
                .update(json)
                .addOnSuccessListener(unused -> listener.onComplete(""))
                .addOnFailureListener(e -> listener.onComplete("Failed to edit post " + e));
    }

    public void deletePlaceReviewById(String placeReviewId, Model.EditPlaceReviewListener listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .document(placeReviewId)
                .update("isDeleted", true,
                        "updateDate", FieldValue.serverTimestamp())
                .addOnSuccessListener(unused -> listener.onComplete(""))
                .addOnFailureListener(e -> listener.onComplete("Failed to delete post"));
    }

    public void getPlaceReviewById(String placeReviewId, Model.GetPlaceReviewById listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .document(placeReviewId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        PlaceReview placeReview = null;
                        if (task.isSuccessful() && task.getResult()!= null && task.getResult().getData() != null){
                            placeReview = placeReview.create(task.getResult().getData());
                        }
                        listener.onComplete(placeReview);
                    }
                });
    }

    public void getPlaceReviewByNameAndUid(String uid, String name, Model.GetPlaceReviewByNameAndUid listener) {
        db.collection(PlaceReview.COLLECTION_NAME)
                .whereEqualTo("uid", uid)
                .whereEqualTo("name", name)
                .whereEqualTo("isDeleted", false)
                .get()
                .addOnCompleteListener(task -> {
                   PlaceReview finalPlaceReview = null;
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        PlaceReview placeReview = PlaceReview.create(doc.getData());
                        if (placeReview != null && !placeReview.getIsDeleted()) {
                            finalPlaceReview = placeReview;
                        }
                    }
                    listener.onComplete(finalPlaceReview);
                });
    }

    public void addUserRole(UserDetails userDetails, Model.AddUserRoleListener listener) {
        Map<String, Object> json = userDetails.toJson();
        db.collection(UserDetails.COLLECTION_NAME)
                .document(userDetails.getUserId())
                .set(json)
                .addOnSuccessListener(unused -> listener.onComplete())
                .addOnFailureListener(e -> listener.onComplete());
    }

    public void getUserDetailsById(String userId, Model.GetUserDetailsListener listener) {
        db.collection(UserDetails.COLLECTION_NAME)
                .document(userId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful() && task.getResult()!= null && task.getResult().getData() != null){
                            UserDetails userDetails = UserDetails.create(task.getResult().getData());
                            listener.onComplete(userDetails);
                            return;
                        }
                        listener.onComplete(null);
                    }
                });
    }

    public void getUserDetailsByUsername(String username, Model.GetUserDetailsListener listener) {
        db.collection(UserDetails.COLLECTION_NAME)
                .whereEqualTo("username", username)
                .get()
                .addOnCompleteListener(task -> {
                    List<UserDetails> list = new LinkedList<UserDetails>();
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot doc : task.getResult()) {
                            UserDetails userDetails = UserDetails.create(doc.getData());
                            if (userDetails != null) {
                                list.add(userDetails);
                            }
                        }
                    }

                    if (list.size() == 0) {
                        listener.onComplete(null);
                    }
                    else {
                        listener.onComplete(list.get(0)); // shouldn't be more than 1
                    }
                });
    }

    public void SetUserDetailsField(String uid, String field, String value, Model.SetUserDetailsFieldListener listener) {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put(field, value);
        // json.put("updateDate", FieldValue.ServerTimestamp()) - if i want to add cache here
        db.collection(UserDetails.COLLECTION_NAME)
                .document(uid)
                .update(json)
                .addOnSuccessListener(unused -> listener.onComplete(true))
                .addOnFailureListener(e -> listener.onComplete(false));
    }

    /**
     * Firebase Storage
     */
    FirebaseStorage storage = FirebaseStorage.getInstance();
    public void saveImage(Bitmap imageBitmap, String imageName, Model.SaveImageListener listener) {
        StorageReference storageRef = storage.getReference();
        StorageReference imgRef = storageRef.child("PlacesReviews/" + imageName);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imgRef.putBytes(data);
        uploadTask.addOnFailureListener(exception -> listener.onComplete(null))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                imgRef.getDownloadUrl().addOnSuccessListener(uri -> {

                    Uri downloadUrl = uri;
                    listener.onComplete(downloadUrl.toString());
                });
            }
        });
    }

    public void saveProfileImage(Bitmap imageBitmap, String imageName, Model.SaveImageListener listener) {
        StorageReference storageRef = storage.getReference();
        StorageReference imgRef = storageRef.child("Profiles/" + imageName);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imgRef.putBytes(data);
        uploadTask.addOnFailureListener(exception -> listener.onComplete(null))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        imgRef.getDownloadUrl().addOnSuccessListener(uri -> {

                            Uri downloadUrl = uri;
                            listener.onComplete(downloadUrl.toString());
                        });
                    }
                });
    }

    /**
     * Authentication
     */
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public boolean isSignedIn(){
        FirebaseUser currentUser = mAuth.getCurrentUser();
        return (currentUser != null);
    }

}
