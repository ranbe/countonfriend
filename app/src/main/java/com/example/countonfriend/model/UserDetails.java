package com.example.countonfriend.model;

import androidx.annotation.NonNull;
import androidx.room.PrimaryKey;

import java.util.HashMap;
import java.util.Map;

public class UserDetails {
    final public static String COLLECTION_NAME = "users";
    public enum Role {
        User,
        Admin
    }

    @PrimaryKey
    @NonNull
    String userId;
    Role role;
    String username;
    String imageUrl;

    public UserDetails(String userId, Role role, String username, String imageUrl) {
        this.userId = userId;
        this.role = role;
        this.username = username;
        this.imageUrl = imageUrl;
    }

    public String getUserId() { return userId; }
    public void setUserId(String userId) { this.userId = userId; }

    public Role getUserRole() { return role; }
    public void  setUserRole(Role role) { this.role = role; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public String getImageUrl() { return imageUrl; }
    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public Map<String, Object> toJson() {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("userId", userId);
        json.put("role", role);
        json.put("username", username);
        json.put("imageUrl", imageUrl);
        return json;
    }

    public static UserDetails create(Map<String, Object> json) {
        String userId = (String) json.get("userId");
        Role role = Role.valueOf(json.get("role").toString());
        String username = (String) json.get("username");
        String imageUrl = (String) json.get("imageUrl");

        UserDetails userDetails = new UserDetails(userId, role, username, imageUrl);
        return userDetails;
    }
}
