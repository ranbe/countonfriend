package com.example.countonfriend.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.core.os.HandlerCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.countonfriend.MyApplication;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Model {
    public static final Model instance = new Model();
    public Executor executor = Executors.newFixedThreadPool(1);
    public Handler mainThread = HandlerCompat.createAsync(Looper.getMainLooper());


    public enum PlaceReviewListLoadingState {
        loading,
        loaded
    }

    MutableLiveData<PlaceReviewListLoadingState> placeReviewListLoadingState = new MutableLiveData<PlaceReviewListLoadingState>();

    public LiveData<PlaceReviewListLoadingState> getPlaceReviewListLoadingState() {
        return placeReviewListLoadingState;
    }

    ModelFirebase modelFirebase = new ModelFirebase();

    private Model() {
        placeReviewListLoadingState.setValue(PlaceReviewListLoadingState.loaded);
        userPlaceReviewListLoadingState.setValue(UserPlaceReviewListLoadingState.loaded);
    }

    MutableLiveData<List<PlaceReview>> placeReviewsList = new MutableLiveData<List<PlaceReview>>();

    public LiveData<List<PlaceReview>> getAll() {
        if (placeReviewsList.getValue() == null) {
            refreshPlaceReviewsList();
        }
        ;
        return placeReviewsList;
    }

    public enum UserPlaceReviewListLoadingState {
        loading,
        loaded
    }

    MutableLiveData<UserPlaceReviewListLoadingState> userPlaceReviewListLoadingState = new MutableLiveData<UserPlaceReviewListLoadingState>();

    public LiveData<UserPlaceReviewListLoadingState> getUserPlaceReviewListLoadingState() {
        return userPlaceReviewListLoadingState;
    }
    MutableLiveData<List<PlaceReview>> userPlaceReviewsList = new MutableLiveData<List<PlaceReview>>();

    public LiveData<List<PlaceReview>> getAllUserReviews(String uid) {
        if (userPlaceReviewsList.getValue() == null) {
            refreshUserPlaceReviewsList(uid);
        }

        ;
        return userPlaceReviewsList;
    }

    public void refreshUserPlaceReviewsList(String uid) {
        userPlaceReviewListLoadingState.setValue(UserPlaceReviewListLoadingState.loading);
        Model.instance.getPlaceReviewsByUid(uid, new Model.GetPlaceReviewsByUid() {
            @Override
            public void onComplete(List<PlaceReview> placeReviews) {
                userPlaceReviewsList.postValue(placeReviews);
                userPlaceReviewListLoadingState.setValue(UserPlaceReviewListLoadingState.loaded);
            }
        });
    }


    public void refreshPlaceReviewsList() {
        placeReviewListLoadingState.setValue(PlaceReviewListLoadingState.loading);

        // get last local update date
        Long lastUpdateDate = MyApplication.getContext().getSharedPreferences("TAG", Context.MODE_PRIVATE).getLong("PlaceReviewsLastUpdateDate", 0);

        executor.execute(() -> {
            List<PlaceReview> stList = AppLocalDb.db.placeReviewDao().getAll();
            placeReviewsList.postValue(stList);
        });

        // firebase get all updates since lastLocalUpdateDate
        modelFirebase.getAllPlaceReviews(lastUpdateDate, new ModelFirebase.GetAllPlaceReviewsListener() {
            @Override
            public void onComplete(List<PlaceReview> list) {
                // add all records to the local db
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Long lud = new Long(0);
                        Log.d("TAG", "fb returned " + list.size());
                        for (PlaceReview placeReview : list) {
                            AppLocalDb.db.placeReviewDao().insertAll(placeReview);
                            if (lud < placeReview.getUpdateDate()) {
                                lud = placeReview.getUpdateDate();
                            }
                        }
                        // update last local update date
                        MyApplication.getContext()
                                .getSharedPreferences("TAG", Context.MODE_PRIVATE)
                                .edit()
                                .putLong("PlaceReviewsLastUpdateDate", lud)
                                .commit();
                        //return all data to caller
                        List<PlaceReview> stList = AppLocalDb.db.placeReviewDao().getAll();
                        placeReviewsList.postValue(stList);
                        placeReviewListLoadingState.postValue(PlaceReviewListLoadingState.loaded);
                    }
                });
            }
        });
    }

    public interface countReviewsListener {
        void onComplete(int count);
    }

    public void countReviews(countReviewsListener listener) {
        modelFirebase.countReviews((count) -> {
            listener.onComplete(count);
        });
    }

    public interface AddPlaceReviewListener {
        void onComplete(String message);
    }

    public void addPlaceReview(PlaceReview placeReview, AddPlaceReviewListener listener) {
        modelFirebase.addPlaceReview(placeReview, (message) -> {
            listener.onComplete(message);
            if (message == "") {
                refreshPlaceReviewsList();
                refreshUserPlaceReviewsList(FirebaseAuth.getInstance().getCurrentUser().getUid());
            }
        });
    }

    public interface EditPlaceReviewListener {
        void onComplete(String message);
    }

    public void editPlaceReview(String currId, PlaceReview placeReview, EditPlaceReviewListener listener) {
        modelFirebase.editPlaceReview(currId, placeReview, (message) -> {
            listener.onComplete(message);
            if (message == "") {
                refreshUserPlaceReviewsList(FirebaseAuth.getInstance().getCurrentUser().getUid());
                refreshPlaceReviewsList();
            }
        });
    }

    public interface DeletePlaceReviewListener {
        void onComplete(String message);
    }

    public void deletePlaceReviewById(String placeReviewId, DeletePlaceReviewListener listener) {
        executor.execute(() -> {
            AppLocalDb.db.placeReviewDao().deleteById(placeReviewId);
        });
        modelFirebase.deletePlaceReviewById(placeReviewId, (message) -> {
            listener.onComplete(message);
            if (message == "") {
                refreshPlaceReviewsList();
                refreshUserPlaceReviewsList(FirebaseAuth.getInstance().getCurrentUser().getUid());
            }
        });
    }

    public interface GetPlaceReviewById {
        void onComplete(PlaceReview placeReview);
    }

    public PlaceReview getPlaceReviewById(String placeReviewId, GetPlaceReviewById listener) {
        modelFirebase.getPlaceReviewById(placeReviewId, listener);
        return null;
    }

    public interface GetPlaceReviewByNameAndUid {
        void onComplete(PlaceReview placeReview);
    }

    public PlaceReview getPlaceReviewByNameAndUid(String uid, String name, GetPlaceReviewByNameAndUid listener) {
        modelFirebase.getPlaceReviewByNameAndUid(uid, name, listener);
        return null;
    }

    public interface GetPlaceReviewsByUid {
        void onComplete(List<PlaceReview> placeReviews);
    }

    public List<PlaceReview> getPlaceReviewsByUid(String uid, GetPlaceReviewsByUid listener) {
        modelFirebase.getPlaceReviewsByUid(uid, listener);
        return null;
    }

    public interface AddUserRoleListener {
        void onComplete();
    }

    public void addUserRole(UserDetails userDetails, AddUserRoleListener listener) {
        modelFirebase.addUserRole(userDetails, () -> {
            listener.onComplete();
        });
    }

    public interface GetUserDetailsListener {
        void onComplete(UserDetails userDetails);
    }

    public void getUserDetailsById(String userId, GetUserDetailsListener listener) {
        modelFirebase.getUserDetailsById(userId, (userDetails) -> {
            listener.onComplete(userDetails);
        });
    }

    public void getUserDetailsByUsername(String username, GetUserDetailsListener listener) {
        modelFirebase.getUserDetailsByUsername(username, (userDetails) -> {
            listener.onComplete(userDetails);
        });
    }

    public interface SetUserDetailsFieldListener {
        void onComplete(boolean succeeded);
    }

    public void SetUserDetailsField(String uid, String field, String value, SetUserDetailsFieldListener listener) {
        modelFirebase.SetUserDetailsField(uid, field, value, (succeeded) -> {
            listener.onComplete(succeeded);
        });
    }

    public interface SaveImageListener {
        void onComplete(String url);
    }

    public void saveImage(Bitmap imageBitmap, String imageName, SaveImageListener listener) {
        modelFirebase.saveImage(imageBitmap, imageName, listener);
    }

    public void saveProfileImage(Bitmap imageBitmap, String imageName, SaveImageListener listener) {
        modelFirebase.saveProfileImage(imageBitmap, imageName, listener);
    }

    /**
     * Authentication
     */

    public boolean isSignedIn() {
        return modelFirebase.isSignedIn();
    }
}