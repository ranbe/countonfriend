package com.example.countonfriend.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FieldValue;

import java.util.HashMap;
import java.util.Map;

@Entity
public class PlaceReview {
    final public static String COLLECTION_NAME = "place_reviews";
    @PrimaryKey
    @NonNull
    String id = "";
    String uid = "";
    String username = "";
    String name = "";
    String description = "";
    String imageUrl;
    Long updateDate = new Long(0);
    boolean isDeleted;

    public void setUpdateDate(Long updateDate) {
        this.updateDate = updateDate;
    }

    public PlaceReview(){}
    public PlaceReview(String name, String id, String description, String uid, String username, boolean isDeleted) {
        this.name = name;
        this.id = id;
        this.description = description;
        this.uid = uid;
        this.username = username;
        this.isDeleted = isDeleted;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    public String getUid() {
        return uid;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }

    public boolean getIsDeleted() { return isDeleted; }
    public void setIsDeleted(boolean isDeleted) { this.isDeleted = isDeleted; }

    public Map<String, Object> toJson() {
        Map<String, Object> json = new HashMap<String, Object>();
        json.put("id",id);
        json.put("name",name);
        json.put("description",description);
        json.put("uid", uid);
        json.put("username", username);
        json.put("updateDate", FieldValue.serverTimestamp());
        json.put("imageUrl",imageUrl);
        json.put("isDeleted",isDeleted);
        return json;
    }

    public static PlaceReview create(Map<String, Object> json) {
        String id = (String) json.get("id");
        String name = (String) json.get("name");
        String description = (String) json.get("description");
        String uid = (String) json.get("uid");
        String username = (String) json.get("username");
        Timestamp ts = (Timestamp)json.get("updateDate");
        Long updateDate = ts.getSeconds();
        String imageUrl = (String)json.get("imageUrl");
        boolean isDeleted = (boolean) json.get("isDeleted");

        PlaceReview placeReview = new PlaceReview(name, id, description, uid, username, isDeleted);
        placeReview.setUpdateDate(updateDate);
        placeReview.setImageUrl(imageUrl);
        return placeReview;
    }

    public Long getUpdateDate() {
        return updateDate;
    }

    public void setImageUrl(String url) {
        imageUrl = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
