package com.example.countonfriend.register;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.countonfriend.R;
import com.example.countonfriend.feed.BaseActivity;
import com.example.countonfriend.login.LoginActivity;
import com.example.countonfriend.model.Model;
import com.example.countonfriend.model.UserDetails;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class RegisterFragment extends Fragment {
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        progressBar = view.findViewById(R.id.register_progressbar);
        progressBar.setVisibility(View.GONE);
        TextView goToLoginBtn = view.findViewById(R.id.register_login_tv);
        goToLoginBtn.setOnClickListener(view1 -> {
            toLoginActivity();
        });
        EditText email = view.findViewById(R.id.register_email_et);
        EditText password = view.findViewById(R.id.register_password_et);
        EditText usernameEt = view.findViewById(R.id.register_usrename_et);

        Button registerBtn = view.findViewById(R.id.register_register_btn);
        registerBtn.setOnClickListener(v -> {
            String emailTxt = email.getText().toString();
            if (!isValidEmail(emailTxt)) {
                email.setError("Not a valid email");
                return;
            }
            String passwordTxt = password.getText().toString();
            if (!isValidPassword(passwordTxt)) {
                password.setError("Password is less than 6 characters");
                return;
            }

            String username = usernameEt.getText().toString();
            if (username.length() < 4) {
                usernameEt.setError("username can't be less than 4 letters");
                return;
            }
            progressBar.setVisibility(View.VISIBLE);

            Model.instance.getUserDetailsByUsername(username, (userDetails) -> {
                if (userDetails != null) {
                    progressBar.setVisibility(View.GONE);
                    usernameEt.setError("Username is already in use, try another one");
                    usernameEt.requestFocus();
                    return;
                }
                else {
                    FirebaseAuth mAuth;
                    mAuth = FirebaseAuth.getInstance();
                    mAuth.createUserWithEmailAndPassword(emailTxt, passwordTxt)
                            .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (task.isSuccessful()) {
                                        Toast.makeText(getContext(), "Registered!.",
                                                Toast.LENGTH_SHORT).show();
                                        FirebaseUser user = task.getResult().getUser();
                                        UserDetails userDetails = new UserDetails(task.getResult().getUser().getUid(), UserDetails.Role.User, username, "");
                                        Model.instance.addUserRole(userDetails, () -> {
                                            progressBar.setVisibility(View.GONE);
                                            toFeedActivity();
                                        });
                                    } else {
                                        progressBar.setVisibility(View.GONE);
                                        String message = task.getException().getMessage().toString();
                                        if (message.contains("email")) {
                                            email.setError(message);
                                            email.requestFocus();
                                        } else if (message.contains("password")){
                                            password.setError(message);
                                            password.requestFocus();
                                        } else {
                                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                }
            });



        });
        return view;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(CharSequence password) {
        return password.length() > 5;
    }

    private void toFeedActivity() {
        Intent intent = new Intent(getContext(), BaseActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void toLoginActivity() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}