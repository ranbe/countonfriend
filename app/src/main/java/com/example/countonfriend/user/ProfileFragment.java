package com.example.countonfriend.user;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.countonfriend.R;
import com.example.countonfriend.model.Model;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class ProfileFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    EditText usernameTv;
    TextView roleTv;
    ProgressBar progressBar;
    Bitmap imageBitmap;
    ImageView userImgImv;
    ImageButton camBtn;
    ImageButton galleryBtn;
    String currUsername;
    Button saveBtn;
    Button backBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile,container, false);

        usernameTv = view.findViewById(R.id.profile_username_tv);
        roleTv = view.findViewById(R.id.profile_role_tv);
        userImgImv = view.findViewById(R.id.profile_user_image_imv);
        progressBar = view.findViewById(R.id.profile_progressbar);
        progressBar.setVisibility(View.VISIBLE);
        String uid = FirebaseAuth.getInstance().getUid();
        Model.instance.getUserDetailsById(uid, (userDetails -> {
            if (userDetails != null) {
                currUsername = userDetails.getUsername();
                usernameTv.setText(userDetails.getUsername());
                roleTv.setText("My role: " + userDetails.getUserRole().toString());
                if (userDetails.getImageUrl() != "") {
                    Picasso.get().load(userDetails.getImageUrl()).into(userImgImv);
                }
                progressBar.setVisibility(View.GONE);
            }
        }));

        camBtn = view.findViewById(R.id.profile_cam_btn);
        galleryBtn = view.findViewById(R.id.profile_gallery_btn);

        camBtn.setOnClickListener(v -> {
            openCam();
        });

        galleryBtn.setOnClickListener(v -> {
            openGallery();
        });
        backBtn = view.findViewById(R.id.profile_back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(usernameTv).navigateUp();
            }
        });
        saveBtn = view.findViewById(R.id.profile_save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String username = usernameTv.getText().toString();
               if (username.length() < 3) {
                   usernameTv.setError("Username should have at least 3 characters");
                   return;
               }

               if (!currUsername.equals(username)) {

                   Model.instance.getUserDetailsByUsername(username, (userDetails) -> {
                       if (userDetails != null) {
                           usernameTv.setError("Username is already in use, try another one");
                           usernameTv.requestFocus();
                           return;
                       } else {
                           save();
                       }
                   });
               }
               else {
                    save();
               }
           }
       });
        return view;
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_FROM_GALLERY);
    }

    private void openCam() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA){
            if (resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                userImgImv.setImageBitmap(imageBitmap);

            }
        }
        else if (requestCode == PICK_IMAGE_FROM_GALLERY){
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        imageBitmap = bitmap;
                        userImgImv.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void save() {
        progressBar.setVisibility(View.VISIBLE);
        saveBtn.setEnabled(false);
        camBtn.setEnabled(false);
        galleryBtn.setEnabled(false);
        backBtn.setEnabled(false);

        String uid = FirebaseAuth.getInstance().getUid();
        Model.instance.SetUserDetailsField(uid, "username", usernameTv.getText().toString(), (succeeded) -> {
            if (succeeded) {
                if (imageBitmap != null) {
                    Model.instance.saveImage(imageBitmap, uid + ".jpg", url -> {
                        Model.instance.SetUserDetailsField(uid, "imageUrl", url, (succeeded_2) -> {
                            if (succeeded_2) {
                                progressBar.setVisibility(View.GONE);
                                Navigation.findNavController(usernameTv).navigateUp();
                            }
                        });
                    });
                } else {
                    progressBar.setVisibility(View.GONE);
                    Navigation.findNavController(usernameTv).navigateUp();
                }
            }
        });
    }
}