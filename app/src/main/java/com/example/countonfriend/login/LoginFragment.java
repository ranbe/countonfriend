package com.example.countonfriend.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.countonfriend.R;
import com.example.countonfriend.feed.BaseActivity;
import com.example.countonfriend.register.RegisterActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class LoginFragment extends Fragment {
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        progressBar = view.findViewById(R.id.login_progressbar);
        progressBar.setVisibility(View.GONE);

        TextView goToRegisterBtn = view.findViewById(R.id.login_register_tv);
        goToRegisterBtn.setOnClickListener(view1 -> {
            toRegisterActivity();
        });

        EditText email = view.findViewById(R.id.login_email_et);
        EditText password = view.findViewById(R.id.login_password_et);

        Button loginBtn = view.findViewById(R.id.login_login_btn);
        loginBtn.setOnClickListener(v -> {
            String emailTxt = email.getText().toString();
            if (!isValidEmail(emailTxt)) {
                email.setError("Not a valid email");
                return;
            }
            String passwordTxt = password.getText().toString();
            if (!isValidPassword(passwordTxt)) {
                password.setError("Password is less than 6 characters");
                return;
            }
            progressBar.setVisibility(View.VISIBLE);
            FirebaseAuth mAuth;
            mAuth = FirebaseAuth.getInstance();
            mAuth.signInWithEmailAndPassword(emailTxt, passwordTxt)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "Authentication succceeded.",
                                    Toast.LENGTH_SHORT).show();
                            toFeedActivity();
                        } else {
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        });
        return view;
    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(CharSequence password) {
        return password.length() > 5;
    }

    private void toFeedActivity() {
        Intent intent = new Intent(getContext(), BaseActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void toRegisterActivity() {
        Intent intent = new Intent(getContext(), RegisterActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}